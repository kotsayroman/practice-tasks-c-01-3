using System;

namespace Task1
{
	public class Vector
	{
		private double _x;
		private double _y;
		private double _z;

		public Vector(double x = 0.0, double y = 0.0, double z = 0.0)
		{
			_x = x;
			_y = y;
			_z = z;
		}

		public Vector(Vector v)
		{
			_x = v._x;
			_y = v._y;
			_z = v._z;
		}

		public override string ToString()
		{
			return String.Format("({0}, {1}, {2})", _x, _y, _z);
		}

		public override bool Equals(Object obj)
	    {
	        if (obj == null)
	        {
	            return false;
	        }
	        
	        Vector v = obj as Vector;
	        
	        if ((Object)v == null)
	        {
	            return false;
	        }

	        return (this == v);
	    }

		public override int GetHashCode()
    	{
        	return Convert.ToInt32(_x) ^ Convert.ToInt32(_y) ^ Convert.ToInt32(_z);
	    }
		
		public double ScalarProduct(Vector v)
		{
			return (_x * v._x + _y * v._y + _z * v._z);
		}

		public double Length()
		{
			return Math.Sqrt(_x * _x + _y * _y + _z * _z);
		}

		public double Angle(Vector v)
		{
			double product = this.Length() * v.Length();
		
			if(product == 0.0)
			{
				throw new DivideByZeroException("One of vectors has zero length!");
			}

			return Math.Acos(this.ScalarProduct(v) / product);
		}

		public Vector CrossProduct(Vector v)
		{
			double x = _y * v._z - _z * v._y;
			double y = _z * v._x - _x * v._z;
			double z = _x * v._y - _y * v._x;

			_x = x;
			_y = y;
			_z = z;

			return this;
		}

		public double TripleProduct(Vector v1, Vector v2)
		{
			return this.ScalarProduct(v1.CrossProduct(v2));
		}

		public Vector Add(Vector v)
		{
			_x += v._x;
			_y += v._y;
			_z += v._z;

			return this;
		}

		public Vector Subtract(Vector v)
		{			
			_x -= v._x;
			_y -= v._y;
			_z -= v._z;

			return this;
		}

	    public static double ScalarProduct(Vector v1, Vector v2)
		{
			return (v1._x * v2._x + v1._y * v2._y + v1._z * v2._z);
		}

		public static double Length(Vector v1)
		{
			return Math.Sqrt(v1._x * v1._x + v1._y * v1._y + v1._z * v1._z);
		}

		public static double Angle(Vector v1, Vector v2)
		{
			double product = Length(v1) * Length(v2);
		
			if(product == 0.0)
			{
				throw new DivideByZeroException("One of vectors has zero length!");
			}

			return Math.Acos(ScalarProduct(v1, v2) / product);
		}

		public static Vector CrossProduct(Vector v1, Vector v2)
		{
			double x = v1._y * v2._z - v1._z * v2._y;
			double y = v1._z * v2._x - v1._x * v2._z;
			double z = v1._x * v2._y - v1._y * v2._x;

			return new Vector(x, y, z);
		}

		public static double TripleProduct(Vector v1, Vector v2, Vector v3)
		{
			return ScalarProduct(v1, CrossProduct(v2, v3));
		}
		
		public static Vector operator+(Vector v1, Vector v2)
		{
			return new Vector(v1._x + v2._x, v1._y + v2._y, v1._z + v2._z);
		}

		public static Vector operator-(Vector v1, Vector v2)
		{
			return new Vector(v1._x - v2._x, v1._y - v2._y, v1._z - v2._z);
		}

		public static bool operator==(Vector v1, Vector v2)
		{
			return ((v1._x == v2._x) && (v1._y == v2._y) && (v1._z == v2._z));
		}

		public static bool operator!=(Vector v1, Vector v2)
		{
			return !(v1 == v2);
		}
	}

	class Program
	{
		static void Main()
		{

			try
			{
				Vector v1 = new Vector(1, 2, 3);
				Vector v2 = new Vector(-1, 2, -3);
				Vector v3 = new Vector(v1);
				Vector v4 = new Vector();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v3 = {0}", v3);
				Console.WriteLine("v4 = {0}", v4);
				Console.WriteLine();

				Console.WriteLine("Is v1 == v2? {0}", v1 == v2);
				Console.WriteLine("Is v1 == v3? {0}", v1 == v3);
				Console.WriteLine();

				Console.WriteLine("Is v1 equel v2? {0}", v1.Equals(v2));
				Console.WriteLine("Is v1 equel v3? {0}", v1.Equals(v3));
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("(static)Length v1 = {0}", Vector.Length(v1));
				
				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("(static)Length v4 = {0}", Vector.Length(v4));
				
				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("Length v1 = {0}", v1.Length());
				
				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("Length v4 = {0}", v4.Length());
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v1 + v2 = {0}", v1 + v2);
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v1.Add(v2) = {0}", v1.Add(v2));
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v1 - v2 = {0}", v1 - v2);
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v1.Subtract(v2) = {0}", v1.Subtract(v2));
				Console.WriteLine();

				Vector v11 = new Vector(v1);
				Vector v22 = new Vector(v2);

				Console.WriteLine("v11 = {0}", v11);
				Console.WriteLine("v22 = {0}", v22);
				Console.WriteLine("(static) v11 * v22 = {0}", Vector.ScalarProduct(v11, v22));
				Console.WriteLine();

				Console.WriteLine("v11 = {0}", v11);
				Console.WriteLine("v22 = {0}", v22);
				Console.WriteLine("(static) v11 x v22 = {0}", Vector.CrossProduct(v11, v22));
				Console.WriteLine();

				Console.WriteLine("v11 = {0}", v11);
				Console.WriteLine("v22 = {0}", v22);
				Console.WriteLine("v3 = {0}", v3);
				Console.WriteLine("(static) v11 * [v22 x v3] = {0}", Vector.TripleProduct(v11, v22, v3));
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v1 * v2 = {0}", v1.ScalarProduct(v2));
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v1 x v2 = {0}", v1.CrossProduct(v2));
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("v3 = {0}", v3);				
				Console.WriteLine("v11 * [v2 x v3] = {0}", v11.TripleProduct(v2, v3));
				Console.WriteLine();

				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v2 = {0}", v2);
				Console.WriteLine("(static) v1 ^ v2 = {0}", Vector.Angle(v1, v2));
				Console.WriteLine("v1 ^ v2 = {0}", v1.Angle(v2));
				
				Console.WriteLine("v1 = {0}", v1);
				Console.WriteLine("v4 = {0}", v4);
				Console.WriteLine("v1 ^ v4 = {0}", v1.Angle(v4));
			}
			catch(DivideByZeroException e)
			{
				Console.WriteLine(e);
			}
			catch
			{			
				Console.WriteLine("Something happens ;(");
			}
		}
	}
}